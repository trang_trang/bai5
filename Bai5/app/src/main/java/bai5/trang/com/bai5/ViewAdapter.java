package bai5.trang.com.bai5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.ViewHolder> {

    private ArrayList<String> nameList;
    private String name;
    private Context context;


    public ViewAdapter(Context context, ArrayList<String> nameList) {
        this.nameList = nameList;
        this.context = context;
    }

    @Override
    public ViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewAdapter.ViewHolder viewHolder, final int i) {
        name = nameList.get(i);
       viewHolder.title.setText(name);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent = new Intent(view.getContext(), ProvinceActivity.class);
                myintent.putExtra("name", name);
                ((Activity) context).startActivityForResult(myintent, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private LinearLayout layout;
        private ImageView img;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            layout = (LinearLayout) view.findViewById(R.id.button_layout);
        }
    }
}
