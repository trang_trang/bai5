package bai5.trang.com.bai5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> nameList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nameList = new ArrayList<>();
        nameList.add("Ha Noi");
        nameList.add("Ha Nam");
        nameList.add("Ha Tinh");
        nameList.add("Nghe An");
        nameList.add("Quang Binh");
        nameList.add("Quang Tri");
        nameList.add("Hue");
        nameList.add("Da Nang");
        nameList.add("Quang Nam");
        nameList.add("Quang Ngai");
        nameList.add("Binh Dinh");
        nameList.add("Tp Ho Chi Minh");
        recyclerView = (RecyclerView) findViewById(R.id.imagegallery);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(layoutManager);
        ViewAdapter adapter = new ViewAdapter(this, nameList);
        recyclerView.setAdapter(adapter);

    }
}
